package com.edilberto.proyectoandroidavanzado

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edilberto.proyectoandroidavanzado.tabs.TabNavigationActivity

class MainWindows : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        next(TabNavigationActivity::class.java,null,true)

    }
}