package com.edilberto.proyectoandroidavanzado.producto.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edilberto.proyectoandroidavanzado.R
import com.edilberto.proyectoandroidavanzado.producto.Producto
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_producto.view.*

class ProductoAdapter(var productos : MutableList<Producto>, val itemCallBackProducto:(item:Producto)->Unit):RecyclerView.Adapter<ProductoAdapter.ProductoAdapterViewHolder>() {

    lateinit var onItemClick:(item:Producto)->Unit

    class ProductoAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(producto: Producto){itemView.tvnombreproducto.text=producto.name
        Picasso.get().load(producto.foto).error(R.drawable.img006).into(itemView.imgproducto)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoAdapterViewHolder {

        val view:View=LayoutInflater.from(parent.context).inflate(R.layout.row_producto,parent,false)
        return ProductoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productos.size
    }

    override fun onBindViewHolder(holder: ProductoAdapterViewHolder, position: Int) {

        val producto = productos[position]

        holder.bind(producto)
        holder.itemView.setOnClickListener {
            println("hola")
            itemCallBackProducto(producto)
       }
    }
}