package com.edilberto.proyectoandroidavanzado.producto

import java.io.Serializable

data class Producto (val id:Int, val name:String,
                     val foto:Int): Serializable {
}

