package com.edilberto.proyectoandroidavanzado

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.edilberto.proyectoandroidavanzado.producto.Producto
import com.edilberto.proyectoandroidavanzado.producto.adapter.ProductoAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val productos = mutableListOf<Producto>()
    lateinit var adaptador: ProductoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cargarinformacion()
        configuraradaptador()
    }

    private fun configuraradaptador() {
        adaptador=ProductoAdapter(productos){
            val bundle = Bundle().apply {
                putSerializable("KEY_PRODUCTO",it)
            }
            val intent=Intent(this,MostrarPantalla::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        recyclerviewproductos.adapter=adaptador
        recyclerviewproductos.layoutManager= LinearLayoutManager(this)

    }
    private fun cargarinformacion() {
        productos.add(Producto(1,"Nicolle S/.30",R.drawable.img001))
        productos.add(Producto(2,"Peter S/.10",R.drawable.img002))
        productos.add(Producto(3,"Alejandra S/.30",R.drawable.img003))
        productos.add(Producto(4,"Victor S/.10",R.drawable.img004))
        productos.add(Producto(5,"Saul S/.10",R.drawable.img005))
        productos.add(Producto(6,"User S/.50",R.drawable.img006))
    }
}