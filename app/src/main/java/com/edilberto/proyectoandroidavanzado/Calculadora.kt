package com.edilberto.proyectoandroidavanzado

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.calculadora.*
import kotlinx.android.synthetic.main.layout_loading.*

class Calculadora : AppCompatActivity(),View.OnClickListener  {

    private val TAG= "CONSOLE"
    private var op1=0
    private var op2=0
    private var op=0.0

    override fun onClick(v: View?) {
        Log.v(TAG, "click")

        //TODO capturar valores
        val mOp1= editTextOp1.text.toString()
        val mOp2= editTextOp2.text.toString()
        Log.v(TAG, "mOp1 $mOp1 mOp2 $mOp2")

        //TODO validar campos
        if(mOp1.isEmpty() || mOp2.isEmpty()){
            return
        }

        op1= editTextOp1.text.toString().toInt()
        op2= editTextOp2.text.toString().toInt()

        Log.v(TAG, "op1 $op1 op2 $op2")

        op = when(v?.id){
            R.id.iviSum -> (op1 sum op2).toDouble()
            R.id.iviSubtract -> (op1 sub op2).toDouble()
            R.id.iviMultiply -> (op1 multiply  op2).toDouble()
            R.id.iviDivide -> op1 divide op2
            else -> 0.0
        }

        Log.v(TAG, "op $op")

        //TODO mostrar resultados
        tviOp.text= "Resultado $op"
    }
    private fun sumar(mOp1:Int, mOp2:Int):Int{
        return mOp1+mOp2
    }

    private fun sumar1(mOp1:Int, mOp2:Int):Int = mOp1+mOp2
    private fun sumar2(mOp1:Int, mOp2:Int)= mOp1+mOp2

    val sumar3:(Int, Int) -> Int = {mOp1, mOp2 ->
        mOp1 + mOp2
    }

    val sumar4 = {mOp1:Int, mOp2:Int -> (mOp1+ mOp2).toDouble()} //sum
    val restar4 = {mOp1:Int, mOp2:Int -> (mOp1- mOp2).toDouble()}
    val multiplicar4 = {mOp1:Int, mOp2:Int -> (mOp1* mOp2).toDouble()}
    val dividir4 = {mOp1:Int, mOp2:Int -> mOp1*1.0/ mOp2}

    private fun restar(mOp1:Int, mOp2:Int)= mOp1-mOp2 // subtract
    private fun multiplicar(mOp1:Int, mOp2:Int)= mOp1*mOp2 // multiply
    private fun dividir(mOp1:Int, mOp2:Int)= mOp1*1.0/mOp2 //divide

    val safeDivide: (Int, Int) -> Double = { numerator, denominator ->
        if (denominator == 0) 0.0 else numerator.toDouble() / denominator
    }

    private infix fun Int.sum(param2:Int) = this + param2
    private infix fun Int.sub(param2:Int) = this - param2
    private infix fun Int.multiply(param2:Int) = this * param2
    private infix fun Int.divide(param2:Int) = this*1.0 / param2

    private fun operation(mOp1:Int, mOp2:Int,f:(Int,Int)->Double):Double{
        return f(mOp1,mOp2)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calculadora)
        iviSum.setOnClickListener(this)
        iviSubtract.setOnClickListener(this)
        iviMultiply.setOnClickListener(this)
        iviDivide.setOnClickListener(this)


    }

}

