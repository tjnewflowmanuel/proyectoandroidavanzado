package com.edilberto.proyectoandroidavanzado.model

import androidx.annotation.Keep
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

@Keep
@IgnoreExtraProperties
data class NoteEntity(val id:String?="",val name:String?="", val direction:String?="", val territory:String?="", val description:String?=""):Serializable {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
                "id" to id,
                "name" to name,
            "direction" to direction,
            "territory" to territory,
                "description" to description
        )
    }
}