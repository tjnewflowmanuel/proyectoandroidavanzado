package com.edilberto.proyectoandroidavanzado

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edilberto.proyectoandroidavanzado.producto.Producto
import kotlinx.android.synthetic.main.activity_mostrar_pantalla.*

class MostrarPantalla : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar_pantalla)

        val bundle:Bundle?= intent.extras
        bundle?.let {
            val producto =it.getSerializable("KEY_PRODUCTO") as Producto
                tvtitulonombre.text=producto.name
            if (tvtitulonombre.text=="Nicolle S/.30"){imageView1.setImageResource(R.drawable.img001)}
            if (tvtitulonombre.text=="Peter S/.10"){imageView1.setImageResource(R.drawable.img002)}
            if (tvtitulonombre.text=="Alejandra S/.30"){imageView1.setImageResource(R.drawable.img003)}
            if (tvtitulonombre.text=="Victor S/.10"){imageView1.setImageResource(R.drawable.img004)}
            if (tvtitulonombre.text=="Saul S/.10"){imageView1.setImageResource(R.drawable.img005)}
            if (tvtitulonombre.text=="User S/.50"){imageView1.setImageResource(R.drawable.img006)}

            println("${producto.name}")
            println("${producto.foto}")
        }
    }
}