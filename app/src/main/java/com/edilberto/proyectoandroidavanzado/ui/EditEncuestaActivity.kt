package com.edilberto.proyectoandroidavanzado.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.edilberto.proyectoandroidavanzado.R
import com.edilberto.proyectoandroidavanzado.model.NoteEntity
import com.edilberto.proyectoandroidavanzado.ui.dialog.EncuestaDialogFragment
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_edit_encuesta.*
import kotlinx.android.synthetic.main.layout_loading.*

class EditEncuestaActivity : AppCompatActivity(), EncuestaDialogFragment.DialogListener {

    private lateinit var mDatabase: DatabaseReference
    private var note: NoteEntity?=null

    private var name:String?=null
    private var direction:String?=null
    private var territory:String?=null
    private var desc:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_encuesta)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        mDatabase= FirebaseDatabase.getInstance().reference
        ui()
        populate()
    }

    private fun ui(){
        btnEditNote.setOnClickListener {
            if(validateForm()){
                editNote()
            }
        }

        btnDeleteNote.setOnClickListener {
            showNoteDialog()
        }
    }

    private fun validateForm():Boolean{
        name= eteName.text.toString()
        direction= eteDirection.text.toString()
        territory= eteTerritory.text.toString()
        desc= eteDesc.text.toString()

        if(name.isNullOrEmpty()){
            return false
        }

        if(desc.isNullOrEmpty()){
            return false
        }

        return true
    }

    private fun populate(){
        note?.let {
            eteName.setText(it?.name)
            eteDirection.setText(it?.direction)
            eteTerritory.setText(it?.territory)
            eteDesc.setText(it?.description)
        }
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

    private fun showNoteDialog(){
        val noteDialogFragment= EncuestaDialogFragment()
        val bundle= Bundle()
        bundle.putString("TITLE","¿Deseas eliminar esta encuesta?")
        bundle.putInt("TYPE",100)

        noteDialogFragment.arguments= bundle
        noteDialogFragment.show(supportFragmentManager,"dialog")
    }


    override fun onPositiveListener(any: Any?, type: Int) {
        note?.let {
            //noteRepository?.deleteNote(it)
            deleteNote(it)
        }
        //finish()
    }

    override fun onNegativeListener(any: Any?, type: Int) {}

    private fun deleteNote(mNote:NoteEntity){
        showLoading()
        mNote.id?.let {
            mDatabase.child("notes").child(it).removeValue(object: DatabaseReference.CompletionListener{
                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {
                    hideLoading()
                    databaseError?.let {
                        showErrorMessage(databaseError.message)
                    }?:run{
                        finish()
                    }
                }
            })
        }
    }

    private fun editNote() {
        showLoading()
        val noteId = note?.id
        val note= NoteEntity(noteId,name,direction,territory,desc)

        noteId?.let {
            mDatabase.child("notes").child(it).updateChildren(note.toMap(),object: DatabaseReference.CompletionListener{
                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {
                    hideLoading()
                    databaseError?.let {
                        showErrorMessage(databaseError.message)
                    }?:run{
                        finish()
                    }
                }
            })
        }
    }

    private fun verifyExtras(){
        intent?.extras?.let {
            note= it.getSerializable("NOTE") as NoteEntity
        }
    }

    private fun showLoading() {
        flayLoading.visibility= View.VISIBLE
    }

    private fun hideLoading() {
        flayLoading.visibility= View.GONE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
