package com.edilberto.proyectoandroidavanzado.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.edilberto.proyectoandroidavanzado.R

class EncuestaViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val iviNote= view.findViewById<ImageView>(R.id.imageViewNote)
    val tviName= view.findViewById<TextView>(R.id.tviName)
    val tviDirection= view.findViewById<TextView>(R.id.tviDirection)
    val tviTerritory= view.findViewById<TextView>(R.id.tviTerritory)
}