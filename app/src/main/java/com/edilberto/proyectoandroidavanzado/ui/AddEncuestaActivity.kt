package com.edilberto.proyectoandroidavanzado.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.edilberto.proyectoandroidavanzado.R
import com.edilberto.proyectoandroidavanzado.model.NoteEntity
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_encuesta.*
import kotlinx.android.synthetic.main.layout_loading.*

class AddEncuestaActivity : AppCompatActivity() {

    private lateinit var mDatabase: DatabaseReference

    private var name:String?=null
    private var direction:String?=null
    private var territory:String?=null
    private var desc:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_encuesta)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mDatabase= FirebaseDatabase.getInstance().reference
        ui()
    }

    private fun ui(){
        btnAddNote.setOnClickListener {
            if(validateForm()){
                addNote()
            }
        }
    }

    private fun addNote(){
        showLoading()
        val noteId = mDatabase.child("notes").push().key
        val note= NoteEntity(noteId,name,desc)
        noteId?.let {
            mDatabase.child("notes").child(it).setValue(note,object:DatabaseReference.CompletionListener{
                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {
                    hideLoading()
                    databaseError?.let {dbError ->
                        showErrorMessage(dbError.message)
                    }?:run{
                        finish()
                    }
                }
            })
        }
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

    private fun clearForm(){
        eteName.error=null
        eteDirection.error=null
        eteTerritory.error=null
        eteDesc.error=null

    }

    private fun validateForm():Boolean{
        clearForm()
        name= eteName.text.toString().trim()
        direction= eteDirection.text.toString().trim()
        territory= eteTerritory.text.toString().trim()
        desc= eteDesc.text.toString().trim()

        if(name.isNullOrEmpty()){
            eteName.error="Campo nombres inválido"
            return false
        }

        if(direction.isNullOrEmpty()){
            eteDirection.error="Campo direccion inválido"
            return false
        }

        if(territory.isNullOrEmpty()){
            eteTerritory.error="Campo territorio inválido"
            return false
        }

        if(desc.isNullOrEmpty()){
            eteDesc.error="Campo descripción inválido"
            return false
        }

        return true
    }

    private fun showLoading() {
        flayLoading.visibility= View.VISIBLE
    }

    private fun hideLoading() {
        flayLoading.visibility= View.GONE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
