package com.edilberto.proyectoandroidavanzado.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.edilberto.proyectoandroidavanzado.Calculadora
import com.edilberto.proyectoandroidavanzado.MainActivity
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.layout_loading.*
import com.edilberto.proyectoandroidavanzado.R
import com.edilberto.proyectoandroidavanzado.tabs.TabNavigationActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private fun showViewLoading() {
        flayLoading.visibility = View.VISIBLE
    }

    private fun hideViewLoading() {
        flayLoading.visibility = View.GONE
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun validateForm(): Boolean {
        val email = editTextTextEmail.text.trim()
        val password = editTextTextPassword.text.trim()

        if (email.isEmpty()) {
            return false
        }

        if (password.isEmpty()) {
            return false
        }
        return true
    }

    private fun updateUI(user: FirebaseUser?){
        Log.v("CONSOLE", "error  $user")
    }

    private fun logIn(){
        val email = editTextTextEmail.text.trim().toString()
        val password = editTextTextPassword.text.trim().toString()
        auth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener(this){ task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    operationCompleted(user)
                } else {
                    showMessage("Exception $task.exception")
                }
            }
    }

    private fun setupUi() {
        buttonLogIn.setOnClickListener {
            if (validateForm()) {
                logIn()
                //superLogIn()
                //superLogInThread()
                //superLogInCR()
            }
        }
    }

    private fun operationCompleted(user: FirebaseUser?) {
        Log.v("CONSOLE", "user $user")
        showMessage("Operation exitosa!")
        startActivity(Intent(this, EncuestaListActivity::class.java))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        auth = Firebase.auth
        setupUi()
        button2.setOnClickListener {
            val intent = Intent(this, Calculadora::class.java)
            startActivity(intent)
        }
        button3.setOnClickListener {
            val intent = Intent(this, TabNavigationActivity::class.java)
            startActivity(intent)
        }
        button4.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}