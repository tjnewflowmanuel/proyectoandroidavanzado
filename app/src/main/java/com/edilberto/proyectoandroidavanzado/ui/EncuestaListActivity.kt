package com.edilberto.proyectoandroidavanzado.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.edilberto.proyectoandroidavanzado.model.NoteEntity
import com.edilberto.proyectoandroidavanzado.ui.adapter.EncuestaAdapter
import com.edilberto.proyectoandroidavanzado.ui.adapter.RecyclerClickListener
import com.edilberto.proyectoandroidavanzado.ui.adapter.RecyclerTouchListener
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import com.edilberto.proyectoandroidavanzado.R
import kotlinx.android.synthetic.main.activity_encuesta_list.*


class EncuestaListActivity : AppCompatActivity() {

    private lateinit var mDatabase: DatabaseReference
    private var encuestaAdapter: EncuestaAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encuesta_list)
        //supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //FirebaseApp.initializeApp(this)
        setUpFirebaseDb()
        ui()
    }

    private fun ui(){
        recyclerViewNotes.layoutManager= LinearLayoutManager(this)
        recyclerViewNotes.setHasFixedSize(true)
        loadNotes()

        //events
        btnAddNote.setOnClickListener {
            goToAddNote()
        }

        recyclerViewNotes.addOnItemTouchListener(
            RecyclerTouchListener(this, recyclerViewNotes,
                object: RecyclerClickListener {
                    override fun onClick(view: View, position: Int) {
                        val note: NoteEntity?=encuestaAdapter?.getItem(position)
                        note?.let {
                            goToNote(note)
                        }
                    }

                    override fun onLongClick(view: View, position: Int) {}
                })
        )
    }

    private fun setUpFirebaseDb(){
        mDatabase= FirebaseDatabase.getInstance().reference
    }

    private fun loadNotes(){
        val notesQuery:Query = mDatabase.child("notes")
        val options= FirebaseRecyclerOptions.Builder<NoteEntity>()
                .setQuery(notesQuery,NoteEntity::class.java)
                .build()

        encuestaAdapter= EncuestaAdapter(options)
        recyclerViewNotes.adapter= encuestaAdapter

    }


    override fun onStart() {
        super.onStart()
        encuestaAdapter?.startListening()
    }

    public override fun onStop() {
        super.onStop()
        encuestaAdapter?.stopListening()
    }

    private fun goToAddNote(){
        startActivity(Intent(this, AddEncuestaActivity::class.java))
    }

    private fun goToNote(note:NoteEntity){
        val bundle= Bundle()
        bundle.putSerializable("NOTE",note)
        val intent= Intent(this,EditEncuestaActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}
