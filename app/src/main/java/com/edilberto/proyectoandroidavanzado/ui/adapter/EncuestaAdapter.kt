package com.edilberto.proyectoandroidavanzado.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.edilberto.proyectoandroidavanzado.R
import com.edilberto.proyectoandroidavanzado.model.NoteEntity
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions

class EncuestaAdapter(options: FirebaseRecyclerOptions<NoteEntity>) : FirebaseRecyclerAdapter<NoteEntity, EncuestaViewHolder>(options) {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): EncuestaViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        return EncuestaViewHolder(inflater.inflate(R.layout.row_encuesta, viewGroup, false))
    }

    override fun onBindViewHolder(holder: EncuestaViewHolder, position: Int, model: NoteEntity) {
        //val item= getRef(position)
        holder.tviName.text= model.name
        holder.tviDirection.text= model.direction
        holder.tviTerritory.text= model.territory
    }
}