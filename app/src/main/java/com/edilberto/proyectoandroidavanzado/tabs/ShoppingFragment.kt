package com.edilberto.proyectoandroidavanzado.tabs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.edilberto.proyectoandroidavanzado.BlankFragment
import com.edilberto.proyectoandroidavanzado.R

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ShoppingFragment: Fragment() {

    private var  listener:OnTabNavListener?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_shopping, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTabNavListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnTabNavListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BlankFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}